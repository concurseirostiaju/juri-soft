// External Dependancies
const mongoose = require('mongoose');

const processoDiarioSchema = new mongoose.Schema({
  id: String,
  numero_antigo: String,
  numero_novo: String,
  diario_sigla: Number,
  diario_nome: String,
  estado: String,
  data_movimentacoes: Date,
  quantidade_movimentacoes: Number
});

module.exports = mongoose.model('processosDiario', processoDiarioSchema);
